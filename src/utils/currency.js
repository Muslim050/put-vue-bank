const formatter = new Intl.NumberFormat('uz-UZ', {currency: 'UZS', style: 'currency'})

export function currency(value) {
  return formatter.format(value)
}